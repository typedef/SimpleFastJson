#ifndef SIMPLE_FAST_JSON_H
#define SIMPLE_FAST_JSON_H

/*
  todo:
  * make it single_header_library

  About lib:

  Lib divided into two parts:
  * SimpleFastJson.h - front-end actual tokenizer, should be smth like jsmn
  fast tokenizing - main job
  * SimpleFastJsonBackend.h - back-end process tokens and outputs better api,
  for getting or setting keys/values, writing json to disk, get actual arrays,
  objects, strings

*/

/*
        #############################
  DOCS: ###       FRONT-END       ###
        #############################
*/

// DOCS: it is possible to define type yourself,
// if for some reasone your platform behave differently

#if !defined(sfj_i8)
typedef char sfj_i8;
#endif

#if !defined(sfj_u8)
typedef unsigned char sfj_u8;
#endif

#if !defined(sfj_i32)
typedef int sfj_i32;
#endif

#if !defined(sfj_u32)
typedef unsigned int sfj_u32;
#endif

#if !defined(sfj_i64)
typedef long long int sfj_i64;
#endif

#if !defined(sfj_u64)
typedef unsigned long long sfj_u64;
#endif

#if !defined(sfj_real)
typedef float sfj_real;
#endif

#if !defined(sfj_null)
#define sfj_null ((void*)0)
#endif

#define SFJ_DEBUG 1
#if SFJ_DEBUG == 1
#define sfj_assert_break() ({volatile int*_some_ptr=sfj_null;*_some_ptr=0;})
#endif

typedef enum SFJTokenType
{
    SFJTokenType_Undefined = 0,

    SFJTokenType_Int,
    SFJTokenType_Int_Exp,
    SFJTokenType_Float,
    SFJTokenType_Float_Exp,
    SFJTokenType_String_Ascii,
    SFJTokenType_String_Unicode,
    SFJTokenType_Bool_True,
    SFJTokenType_Bool_False,
    SFJTokenType_Null,
    SFJTokenType_Array,
    SFJTokenType_Object,

    SFJTokenType_Count,
} SFJTokenType;

const char* sfj_token_type_to_string(SFJTokenType type);

typedef struct SFJToken
{
    SFJTokenType TokenType;
    sfj_i32 Parent;
    union
    {
        sfj_i32 Start;
        sfj_i32 StartInd;
    };

    union
    {
        sfj_i32 End;
        sfj_i32 EndInd;
    };
} SFJToken;

typedef enum SFJErrorType
{
    SFJErrorType_None = 0,
    SFJErrorType_NotEnoughtMemory,
    SFJErrorType_NotValidJson,
    SFJErrorType_HexInvalid,
    SFJErrorType_EscapeCharNotAllowed,
    SFJErrorType_MistakeInParser,
    SFJErrorType_WrongStartCharacter,

    // WrongInputParams
    SFJErrorType_TokensArrayIsNotAllocated,
} SFJErrorType;

const char* sfj_error_type_to_string(SFJErrorType type);

typedef struct SFJSetting
{
    const char* pText;
    sfj_u64 TextLength;
    sfj_u64 TokensCount;
    SFJToken* aTokens;
    SFJErrorType ErrorType;
} SFJSetting;

void sfj_parse_json(SFJSetting* pSetting);
sfj_i32 sfj_get_i32();


/*
        #############################
  DOCS: ###    SFJ_INCLUDE_IMPL   ###
        #############################
*/

#if defined(SFJ_INCLUDE_IMPL)

const char*
sfj_token_type_to_string(SFJTokenType type)
{
    switch (type)
    {
    case SFJTokenType_Undefined: return "Undefined";

    case SFJTokenType_Int: return "Int";
    case SFJTokenType_Int_Exp: return "Int (Exp)";
    case SFJTokenType_Float: return "Float";
    case SFJTokenType_Float_Exp: return "Float (Exp)";
    case SFJTokenType_String_Ascii: return "String (Ascii)";
    case SFJTokenType_String_Unicode: return "String (Unicode)";
    case SFJTokenType_Bool_True: return "Bool_True";
    case SFJTokenType_Bool_False: return "Bool_False";
    case SFJTokenType_Null: return "Null";
    case SFJTokenType_Array: return "Array";
    case SFJTokenType_Object: return "Object";

    case SFJTokenType_Count: return "Count";
    }

    return sfj_null;
}

const char*
sfj_error_type_to_string(SFJErrorType type)
{
    switch (type)
    {
    case SFJErrorType_None: return "None";
    case SFJErrorType_NotEnoughtMemory: return "NotEnoughtMemory";
    case SFJErrorType_NotValidJson: return "NotValidJson";
    case SFJErrorType_HexInvalid: return "HexInvalid";
    case SFJErrorType_EscapeCharNotAllowed: return "EscapeCharNotAllowed";
    case SFJErrorType_MistakeInParser: return "MistakeInParser";
    case SFJErrorType_WrongStartCharacter: return "WrongStartCharacter";

    case SFJErrorType_TokensArrayIsNotAllocated: return "TokensArrayIsNotAllocated";
    }

    return sfj_null;
}

static void
_sfj_add_token(SFJToken* aTokens, sfj_i32* pCurrentToken, SFJToken token)
{
    sfj_i64 current = *pCurrentToken;
    aTokens[current] = token;
    *pCurrentToken = (current + 1);
}

static sfj_i32 gSfjInsideCounter = 0;
sfj_i32 sfj_get_i32() {return gSfjInsideCounter;}

static sfj_u32
sfj_str4_to_u32(const char* ptr)
{
    sfj_u32* pU32 = (sfj_u32*) ptr;
    return *pU32;
}

/* todo:
 * parse array
 * parse ascii escape char \t \r in string
 * parse unicode char inside string
*/
void
sfj_parse_json(SFJSetting* pSetting)
{
    const char* text = pSetting->pText;
    SFJToken* aTokens = pSetting->aTokens;
    sfj_u64 textLength = pSetting->TextLength;
    sfj_u64 tokensCount = pSetting->TokensCount;

    sfj_i32 currentToken = 0;
    SFJErrorType error = SFJErrorType_None;

    sfj_u32 trueAsU32 = sfj_str4_to_u32("true");
    sfj_u32 nullAsU32 = sfj_str4_to_u32("null");
    // f will be known value at this time
    sfj_u32 falsAsU32 = sfj_str4_to_u32("alse");

    //todo: validate
    if (!aTokens)
    {
        error = SFJErrorType_TokensArrayIsNotAllocated;
        goto SFJParseJsonEndLabel;
    }

// note: defines

#define NextToken(passedPtr, passedChar)	\
    ++pos;					\
    ++passedPtr;				\
    passedChar = *passedPtr;

#define SkipDigits(ptr, c, atLeastOne)		\
    {						\
        sfj_u8 uc = (sfj_u8) (c - '0');		\
        while (uc < 10 && c != '\0')		\
        {					\
            NextToken(ptr, c);			\
            uc = (sfj_u8) (c - '0');		\
            atLeastOne = 1;			\
        }					\
    }

#define ReturnError(errType) error = errType; goto SFJParseJsonEndLabel;

#define IsHex(c)					\
    ({							\
        sfj_i32 isHex = (c >= '0' && c <= '9') ||	\
            (c >= 'A' && c <= 'Z') ||			\
            (c >- 'a' && c <= 'z');			\
        isHex;						\
    })

    // parser state
    sfj_i32 pos = 0;
    char* ptr = (char*) text;
    char c = *ptr;
    sfj_i32 parent = -1;

    // note: skip("\t \n") and validate(first character) it should be { or [
    // otherwise return error error = SFJErrorType_NotValidJson;
    while ((c == ' ' || c == '\t'  || c == '\r' || c == '\n') && c != '\0')
    {
        NextToken(ptr, c);
    }
    if (c != '{' && c != '[')
    {
        ReturnError(SFJErrorType_WrongStartCharacter);
    }

    while (c != '\0')
    {
        while (c == ' ' || c == '\t'  || c == '\r')
        {
            NextToken(ptr, c);
        }

        if ((currentToken + 1) >= tokensCount)
        {
            ReturnError(SFJErrorType_NotEnoughtMemory);
        }

        switch (c)
        {
        case ',':
        case ':':
        case '\n':
            NextToken(ptr, c);
            break;

        case '{':
        {
            SFJToken token = {
                .TokenType = SFJTokenType_Object,
                .StartInd = currentToken,
                .EndInd = -1,
                .Parent = parent,
            };

            parent = currentToken;
            _sfj_add_token(aTokens, &currentToken, token);

            NextToken(ptr, c);

            break;
        }

        case '}':
        {

            SFJToken prevToken = aTokens[currentToken - 1];
            if (prevToken.Parent == -1)
            {
                ReturnError(SFJErrorType_NotValidJson);
            }

            while (1)
            {
                ++gSfjInsideCounter;

                SFJToken* pObjToken = &aTokens[prevToken.Parent];
                sfj_i32 isObjRoot = (pObjToken->Start != -1 && pObjToken->End == -1);
                if (isObjRoot)
                {
                    pObjToken->EndInd = currentToken;
                    parent = pObjToken->Parent;
                    break;
                }

                prevToken = *pObjToken;
            }

            NextToken(ptr, c);

            break;
        }

        case '[':
        {
            SFJToken token = {
                .TokenType = SFJTokenType_Array,
                .Start = currentToken,
                .End = -1,
                .Parent = parent,
            };

            parent = currentToken;
            _sfj_add_token(aTokens, &currentToken, token);

            NextToken(ptr, c);

            break;
        }

        case ']':
        {
            SFJToken prevToken = aTokens[currentToken - 1];
            if (prevToken.Parent == -1)
            {
                ReturnError(SFJErrorType_NotValidJson);
            }

            while (1)
            {
                ++gSfjInsideCounter;

                SFJToken* pArrObj = &aTokens[prevToken.Parent];
                sfj_i32 isObjRoot = (pArrObj->Start != -1 && pArrObj->End == -1);
                if (isObjRoot)
                {
                    pArrObj->EndInd = currentToken;
                    parent = pArrObj->Parent;
                    break;
                }

                prevToken = *pArrObj;
            }

            NextToken(ptr, c);

            break;
        }

        case '"':
        {
            // note: skip first \"
            NextToken(ptr, c);

            sfj_i32 startPos = pos;
            sfj_i32 isUnicodeString = 0;

            while (c != '\0')
            {
                if (c == '"')
                {
                    if (!isUnicodeString)
                    {
                        SFJToken token = {
                            .TokenType = SFJTokenType_String_Ascii,
                            .Start = startPos,
                            .End = pos - 1,
                            .Parent = parent
                        };

                        _sfj_add_token(aTokens, &currentToken, token);
                    }
                    else
                    {
                        SFJToken token = {
                            .TokenType = SFJTokenType_String_Unicode,
                            .Start = startPos,
                            .End = pos - 1,
                            .Parent = parent
                        };

                        _sfj_add_token(aTokens, &currentToken, token);
                    }

                    break;
                }
                else if (c == '\\')
                {
                    // DOCS: escaped characters
                    NextToken(ptr, c);

                    /*
                      \0 Null;
                      \a Bell;
                      \b Backspace;
                      \t Horizontal Tabulation;
                      \n New Line;
                      \v Vertical Tabulation;
                      \f Form Feed;
                      \r Carriage Return.
                    */
                    switch (c)
                    {

                    case 't' : case 'n' : case 'r' :
                    case 'b' : case 'v' : case 'f' :
                    case '\"': case '\'': case '\\':
                    case '/':
                        break;

                        // note: handle unicode
                    case 'u':
                    {
                        for (sfj_i32 ui = 0;
                             (ui < 4 && c != '\0');
                             ++ui)
                        {
                            sfj_i32 isHex = IsHex(c);
                            if (!isHex)
                            {
                                ReturnError(SFJErrorType_HexInvalid);
                            }
                        }

                        isUnicodeString = 1;

                        break;
                    }

                    default:
                        ReturnError(SFJErrorType_EscapeCharNotAllowed);
                        break;
                    }
                }

                NextToken(ptr, c);
            }

            // not sure we need this
            NextToken(ptr, c);

            break;
        }

        case 't':
        {
            sfj_i32 startPos = pos;

            sfj_u32 ptrU32 = sfj_str4_to_u32(ptr);
            sfj_i32 isEqual = (ptrU32 == trueAsU32);
            if (!isEqual)
            {
                ReturnError(SFJErrorType_NotValidJson);
            }

            pos += 5;
            ptr += 5;
            c = *ptr;

            SFJToken token = {
                .TokenType = SFJTokenType_Bool_True,
                .Start = startPos,
                .End = startPos + 3,
                .Parent = parent
            };

            _sfj_add_token(aTokens, &currentToken, token);

            //NextToken(ptr, c);

            break;
        }

        case 'f':
        {
            sfj_i32 startPos = pos;

            ++pos;
            ++ptr;
            sfj_u32 ptrU32 = sfj_str4_to_u32(ptr);

            sfj_i32 isEqual = (ptrU32 == falsAsU32);
            if (!isEqual)
            {
                ReturnError(SFJErrorType_NotValidJson);
            }

            pos += 5;
            ptr += 5;
            c = *ptr;

            SFJToken token = {
                .TokenType = SFJTokenType_Bool_False,
                .Start = startPos,
                .End = startPos + 4,
                .Parent = parent
            };

            _sfj_add_token(aTokens, &currentToken, token);

            //NextToken(ptr, c);

            break;
        }

        case 'n':
        {
            sfj_i32 startPos = pos;
            sfj_u32 ptrU32 = sfj_str4_to_u32(ptr);
            sfj_i32 isEqual = ((ptrU32 ^ nullAsU32) == 0);
            if (!isEqual)
            {
                ReturnError(SFJErrorType_NotValidJson);
            }

            ptr += 4;
            pos += 4;
            c = *ptr;

            SFJToken token = {
                .TokenType = SFJTokenType_Null,
                .Start = startPos,
                .End = pos - 1,
                .Parent = parent
            };

            _sfj_add_token(aTokens, &currentToken, token);

            break;
        }

        case '-':
        case '0':
        case '1': case '2': case '3':
        case '4': case '5': case '6':
        case '7': case '8': case '9':
        {
            // note: cost ~300 mb/s
            sfj_i32 startPos = pos;
            SFJTokenType type = SFJTokenType_Int;

            sfj_i32 sign = 1;
            if (c == '-')
            {
                sign = -1;
                NextToken(ptr, c);
            }

            //
            /*
              at least should be 0
              1234
              1234.5
              0.5
            */

            sfj_i32 correctDigitsPart = 0;
            // note: skip int part first
            SkipDigits(ptr, c, correctDigitsPart); // 0000 1010; == 0xA 0X0A0A0A0A

            if (!correctDigitsPart)
            {
                ReturnError(SFJErrorType_NotValidJson);
            }

            /* note: fraction part
               fraction:
               ""
               '.' digits
            */
            if (c == '.')
            {
                type = SFJTokenType_Float;
                NextToken(ptr, c);
                correctDigitsPart = 0;
                SkipDigits(ptr, c, correctDigitsPart);
            }

            /* note: exponent part
               [Ee][-+]\d+
            */
            if (c == 'E' || c == 'e')
            {
                if (type == SFJTokenType_Float)
                    type = SFJTokenType_Float_Exp;
                else
                    type = SFJTokenType_Int_Exp;

                NextToken(ptr, c);

                sfj_i32 isCorrectExp = (c == '-' || c == '+');
                if (!isCorrectExp)
                {
                    ReturnError(SFJErrorType_NotValidJson);
                }

                NextToken(ptr, c);

                SkipDigits(ptr, c, isCorrectExp);
                if (!isCorrectExp)
                {
                    ReturnError(SFJErrorType_NotValidJson);
                }
            }

            SFJToken token = {
                .TokenType = type,
                .Start = startPos,
                .End = pos - 1,
                .Parent = parent,
            };

            _sfj_add_token(aTokens, &currentToken, token);
            break;
        }

        default:
        {
            //_vbreak("never get here", __FILE__, __LINE__, "BREAK IT ALL!!!");
            NextToken(ptr, c);
            break;
        }

        }

    }

SFJParseJsonEndLabel:
    // note: maybe put it inside struct Output
    pSetting->ErrorType = error;
    pSetting->TokensCount = currentToken;
}

#endif // SFJ_INCLUDE_IMPL


#endif // SIMPLE_FAST_JSON_H
